<?php foreach ($variables['matches'] as $day => $championship) : ?>
<div class="livescore-day-wrap">
  <h4><?php print $day; ?></h4>
    <?php foreach ($championship as $header => $matches) : ?>
      <h2><?php print $header; ?></h2>
      <?php foreach ($matches as $match) : ?>
        <div class="livescore-wrap" id="livescore<?php print $match['match_id']; ?>">
          <div class="livescore-match-body"> 
            <div class="livescore-teams-wrap">
              <span class="home_team"><?php print $match['home_team']; ?></span>
              <span class="guest_team"><?php print $match['guest_team']; ?></span>
            </div>
            <div class="livescore-set-details">
              <?php $set_no = 1;
                    while(($set_no < 6)) :
              ?>
                <div class="livescore-set-wrap">

                  <span class="set<?php print $set_no; ?>_home">
                    <?php if (!empty($match['data']['set' . $set_no . '_home']) || !empty($match['data']['set' . $set_no . '_guest'])) : ?>
                      <?php print $match['data']['set' . $set_no . '_home']; ?>
                    <?php endif; ?>
                  </span>
                  <span class="set<?php print $set_no; ?>_guest">
                    <?php if (!empty($match['data']['set' . $set_no . '_home']) || !empty($match['data']['set' . $set_no . '_guest'])) : ?>
                      <?php print $match['data']['set' . $set_no . '_guest']; ?>
                    <?php endif; ?>
                  </span>
                  <?php if (!empty($match['data']['set' . $set_no . '_duration'])) : ?>
                    <span class="set<?php print $set_no; ?>_duration"><?php print $match['data']['set' . $set_no . '_duration']; ?> <?php print t('min'); ?></span>
                  <?php endif; ?>

                </div>
              <?php  $set_no++; endwhile; ?>
            </div>
            <div class="livescore-result-wrap">
              <span class="result_home"><?php print  $match['result_home']; ?></span>
              <span class="result_guest"><?php print $match['result_guest']; ?></span>
            </div>
              <hr />
            <div class="livescore-match-info">
              <span><?php print t('Match start'); ?>: <?php print $match['display_time']; ?></span>
              <span class="spectators">Tilskuere: <?php print $match['spectators']; ?></span>
            </div>
          </div>
        </div>
      <?php endforeach; ?>
    <?php endforeach; ?>
  </div>
<?php endforeach; ?>